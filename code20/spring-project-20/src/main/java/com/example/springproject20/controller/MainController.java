package com.example.springproject20.controller;

import java.util.ArrayList;
import java.util.List;

import com.example.springproject20.model.Comment;
import com.example.springproject20.repository.CommentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @Autowired
    CommentRepository commentRepository;

    public List<Comment> getAllProduct(){
        return commentRepository.findAll();
    }

    @GetMapping({"/", "/home"})
    public String displayItems(Model model){
        model.addAttribute("itemsList", new ArrayList<Comment>(getAllProduct()));
        return "index";
    }
}
