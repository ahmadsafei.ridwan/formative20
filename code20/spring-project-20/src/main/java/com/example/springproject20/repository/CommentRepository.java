package com.example.springproject20.repository;

import java.util.List;

import com.example.springproject20.model.Comment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer>{
    Comment findById(int id);
	List<Comment> findAll();
	void deleteById(int id);
	// Comment save(Comment city);
}
