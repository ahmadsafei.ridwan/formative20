package com.example.springproject20.controller;

import java.util.ArrayList;
import java.util.List;

import com.example.springproject20.model.Comment;
import com.example.springproject20.repository.CommentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerAPI {
    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/allcomments")
    public List<Comment> getAllComments(){
        return commentRepository.findAll();
    }

    @PostMapping(value = "/import-items", consumes = "application/json")
    public String importItem(@RequestBody ArrayList<Comment> comment){
        for (Comment cmt : comment) {
            commentRepository.save(cmt);
        }
        return "Success";
    }
}
