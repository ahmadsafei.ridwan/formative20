package com.example.springproject20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringProject20Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringProject20Application.class, args);
	}

}
