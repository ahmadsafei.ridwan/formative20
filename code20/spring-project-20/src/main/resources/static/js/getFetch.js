const url = "https://jsonplaceholder.typicode.com/posts/1/comments";
const urlAPI = "http://localhost:8080/import-items";

const items = fetch(url)
    .then((response) => response.json())
    .then((data) => {
        let option = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        try {
            fetch(urlAPI, option);
        } catch (error) {
            console.log(error);
        } 
});
