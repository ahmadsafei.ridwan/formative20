<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <!-- <script src="js/getFetch.js"></script> -->
    <title>Formative20</title>
</head>

<body>
    <h1 style="text-align: center;">Formative 20</h1>
    <div class="container">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Body</th>
                    <th scope="col">Post ID</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${itemsList}">
                    <tr>
                        <th scope="row">${item.getId()}</th>
                        <td>${item.getName()}</td>
                        <td>${item.getEmail()}</td>
                        <td>${item.getBody()}</td>
                        <td>${item.getPostId()}</td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <div class="container">
            <button type="button" class="btn btn-danger" onclick="fetchFunction()">Fetch</button>
            <button type="button" class="btn btn-success" onclick="show()">Show</button>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script>
    <script>
        function fetchFunction() {
            const url = "https://jsonplaceholder.typicode.com/posts/1/comments";
            const urlAPI = "http://localhost:8080/import-items";

            const items = fetch(url)
                .then((response) => response.json())
                .then((data) => {
                    let option = {
                        method: "POST",
                        body: JSON.stringify(data),
                        headers: {
                            "Content-type": "application/json; charset=UTF-8"
                        }
                    }
                    try {
                        fetch(urlAPI, option);
                    } catch (error) {
                        console.log(error);
                    }
                });
            alert("fetch berhasil");
        }

        function show() {
            location.replace("http://localhost:8080/home");
        }

        // async function getAPI(url) {
        //     const response = await fetch(url);
        //     var data = await response.json();
        //     console.log(data);
        // }

        // getAPI("http://localhost:8080/allcomments")
    </script>
</body>

</html>